import { TestBed } from '@angular/core/testing';

import { HostingBrowserService } from './hosting-browser.service';

describe('HostingBrowserService', () => {
  let service: HostingBrowserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HostingBrowserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
