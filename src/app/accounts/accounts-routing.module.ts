import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountsComponent } from './accounts.component';
import { AccountAddComponent } from './add/add.component';
import { AccountEditComponent } from './edit/edit.component';

const routes: Routes = [
  {
    path: '',
    component: AccountsComponent,
    data: {
      title: 'Accounts List',
    },
  },
  {
    path: 'add',
    component: AccountAddComponent,
    data: {
      title: 'New Account',
    },
  },

  {
    path: 'edit/:id',
    component: AccountEditComponent,
    data: {
      title: 'Account Edit',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountsRoutingModule {}
