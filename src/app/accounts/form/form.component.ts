import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Account } from '../../../shared/entities/account.entity';

@Component({
  selector: 'app-accounts-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class AccountFormComponent implements OnInit {
  constructor() {}

  account: Account;

  @Input() accountFormCommon: FormGroup;

  ngOnInit(): void {}
}
