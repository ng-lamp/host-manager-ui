import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountFormComponent } from './form/form.component';
import { AccountEditComponent } from './edit/edit.component';
import { AccountAddComponent } from './add/add.component';
import { AccountsRoutingModule } from './accounts-routing.module';

@NgModule({
  declarations: [
    AccountFormComponent,
    AccountEditComponent,
    AccountAddComponent,
  ],
  imports: [CommonModule, AccountsRoutingModule],
})
export class AccountsModule {}
