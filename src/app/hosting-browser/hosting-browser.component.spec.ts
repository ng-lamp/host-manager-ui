import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HostingBrowserComponent } from './hosting-browser.component';

describe('HostingBrowserComponent', () => {
  let component: HostingBrowserComponent;
  let fixture: ComponentFixture<HostingBrowserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HostingBrowserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HostingBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
