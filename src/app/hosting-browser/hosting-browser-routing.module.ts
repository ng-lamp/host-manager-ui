import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HostingBrowserComponent } from './hosting-browser.component';

const routes: Routes = [
  {
    path: '',
    component: HostingBrowserComponent,
    data: {
      title: 'Browse Accounts',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HostingBrowserRoutingModule {}
