import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HostingBrowserRoutingModule } from './hosting-browser-routing.module';
import { HostingBrowserComponent } from './hosting-browser.component';
import { UserEditorModule } from '../user-editor/user-editor.module';

@NgModule({
  declarations: [HostingBrowserComponent],
  imports: [CommonModule, HostingBrowserRoutingModule, UserEditorModule],
})
export class HostingBrowserModule {}
