import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { HostingBrowserService } from '../hosting-browser.service';
import { User } from '../../shared/entities/user.entity';
import { Router } from '@angular/router';
import { Account } from '../../shared/entities/account.entity';
import * as _ from 'lodash';

const trailingDot = /\.$/;
@Component({
  // selector: 'app-hosting-browser',
  templateUrl: './hosting-browser.component.html',
  styleUrls: ['./hosting-browser.component.scss'],
})
export class HostingBrowserComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly hostingBrowserService: HostingBrowserService
  ) {}
  public now = new Date();
  public emptyName = {
    user: { username: '--' },
    account: { name: '--' },
    domain: { name: '--' },
    virtualHosts: { name: '--' },
  };

  public hmUsers$: Observable<User[]>;
  private refresh$ = new BehaviorSubject<any>('');

  public countUserVirtualHosts(user: User) {
    let nAccount = 0;
    let nDomain = 0;
    let nVhost = 0;

    user.accounts.forEach((account) => {
      nAccount++;

      account.domains.forEach((domain) => {
        nDomain++;

        domain.virtualHosts.forEach((virtualHost) => {
          nVhost++;
        });
      });
    });

    return Math.max(nAccount, nDomain, nVhost, 1);
  }

  public countAccountVirtualHosts(account: Account) {
    let nDomain = 0;
    let nVhost = 0;

    account.domains.forEach((domain) => {
      nDomain++;

      domain.virtualHosts.forEach((virtualHost) => {
        nVhost++;
      });
    });

    return Math.max(nDomain, nVhost, 1);
  }

  public combineFqdn(names: string[]): string {
    const result: string[] = [];

    for (let name of names) {
      name = _.trim(name.toLowerCase() || '@');

      if (trailingDot.test(name)) {
        result.push(name.replace(trailingDot, ''));
        break;
      } else if (name !== '@') {
        result.push(name);
      }
    }

    return result.join('.');
  }

  ngOnInit(): void {
    // this.hmUsers$ = this.hostingBrowserService.findAll();
    this.hmUsers$ = this.refresh$.pipe(
      switchMap(() => this.hostingBrowserService.findAll())
    );

    this.hmUsers$.subscribe();
  }
}
