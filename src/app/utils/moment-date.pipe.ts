import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import * as moment from 'moment-timezone';
/**
 * A moment timezone pipe to support parsing based on time zone abbreviations
 * covers all cases of offset variation due to daylight saving.
 *
 * Same API as DatePipe with additional timezone abbreviation support
 * Official date pipe dropped support for abbreviations names from Angular V5
 * Credits: https://medium.com/@markuretsky/extend-angular-datepipe-for-timezone-abbreviations-support-9b65fa0807fb
 */
@Pipe({
  name: 'momentDate',
})
export class MomentDatePipe extends DatePipe implements PipeTransform {
  constructor(@Inject(LOCALE_ID) locale: string) {
    super(locale);
  }

  // Silent TypeScript: Type 'string' is not assignable to type 'null'.
  transform(
    value: null | undefined,
    format?: string,
    timezone?: string,
    locale?: string
  ): null;

  transform(
    value: Date | string | number | null,
    format?: string,
    timezone?: string,
    locale?: string
  ): string {
    const timezoneOffset = moment(value).tz(timezone).format('Z');
    return super.transform(value, format, timezoneOffset, locale);
  }
}
