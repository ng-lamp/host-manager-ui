import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserEditorComponent } from './user-editor.component';

const routes: Routes = [
  {
    path: '',
    component: UserEditorComponent,
    data: {
      title: 'User Editor',
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserEditorRoutingModule {}
