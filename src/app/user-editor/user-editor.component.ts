import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { merge, Observable, Subject, timer } from 'rxjs';
import {
  debounce,
  debounceTime,
  distinctUntilChanged,
  multicast,
  skip,
  take,
  throttleTime,
} from 'rxjs/operators';
import { User } from '../../shared/entities/user.entity';
import { UsersService } from '../users.service';
import * as _ from 'lodash';
import { ModalDirective } from 'ngx-bootstrap/modal';

// const compare = (a, b, keys) =>
//   _.isMatch(
//     // check deep equality
//     a, // get properties from a
//     _.pick(b, keys) // get properties from b
//   );

// const intersection = (o1: any, o2: any): any => {
//   return Object.keys(o1).filter({}.hasOwnProperty.bind(o2));
// };

const editedProperties = (a, b) =>
  _.reduce(
    a,
    (result, value, key) =>
      _.isEqual(value, b[key]) ? result : result.concat({ [key]: value }),
    []
  );

@Component({
  selector: 'app-user-editor',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.scss'],
})
export class UserEditorComponent implements OnInit {
  @ViewChild('confirmLeaveModal') public confirmLeaveModal: ModalDirective;

  isProcessing: Boolean;
  params: Params;
  user$: Observable<User>;
  user: User;
  managers$: Observable<User[]>;
  managers: User[];
  formEdited: [];
  debounceTimer: number = 0;

  constructor(
    private _usersService: UsersService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.params = params;
    });

    if (this.params.id) {
      this.getUser().then(() => {});
      this.getManagers();
      this.userForm = new FormGroup({
        id: new FormControl(null),
        username: new FormControl(null, [
          // Validators.required,
          // Validators.minLength(4),
          // Validators.maxLength(32),
        ]),
        email: new FormControl(null, [Validators.required, Validators.email]),
        phoneLandline: new FormControl(null),
        phoneMobile: new FormControl(null),
        info: new FormControl(null),
        isActive: new FormControl(null),
        managerId: new FormControl(null),
        createdAt: new FormControl(null),
        updatedAt: new FormControl(null),
      });
    } else {
      // adding new user
    }

    this.formEdited = [];
  }

  get username() {
    return this.userForm.get('username')!;
  }

  get email() {
    return this.userForm.get('email')!;
  }

  userForm: FormGroup;

  async getUser() {
    this.isProcessing = true;

    this.user$ = this._usersService.getOne(this.params.id);
    this.user$.subscribe((apiResponse: User) => {
      this.isProcessing = false;

      this.user = apiResponse;

      if (apiResponse) {
        this.userForm.get('id').setValue(apiResponse.id);
        this.userForm.get('username').setValue(apiResponse.username);
        this.userForm.get('email').setValue(apiResponse.email);
        this.userForm.get('phoneLandline').setValue(apiResponse.phoneLandline);
        this.userForm.get('phoneMobile').setValue(apiResponse.phoneMobile);
        this.userForm.get('info').setValue(apiResponse.info);
        this.userForm.get('isActive').setValue(apiResponse.isActive);
        this.userForm.get('managerId').setValue(apiResponse.managerId);
        this.userForm.get('createdAt').setValue(apiResponse.createdAt);
        this.userForm.get('updatedAt').setValue(apiResponse.updatedAt);
      }

      this.userForm.valueChanges
        .pipe(
          distinctUntilChanged(), //  execute only if a different value is emitted
          // debounceTime(250), // Wait for 250ms without being called
          throttleTime(1000, undefined, { leading: true, trailing: true }) // as debounceTime doesn't have leading option, this feels more snappy
          // multicast(new Subject(), (s) =>
          //   merge(s.pipe(take(1)), s.pipe(skip(1), debounceTime(200)))
          // )
          // debounce(() => timer(this.debounceTimer))
        )
        .subscribe((newForm) => {
          this.formEdited = editedProperties(
            newForm,
            this.user
            // _.omit(this.user, ['deletedAt', 'accounts', 'manager']),
          );
          console.info('formEdited', this.formEdited);
        });
    });
  }

  async getManagers() {
    this.managers$ = this._usersService.getManagers();
    this.managers$.subscribe((apiResponse: User[]) => {
      this.managers = apiResponse.filter(
        (manager) => manager.id != this.user.id
      );
    });
  }

  onSubmit() {}

  onCancel() {
    this._router.navigate(['hosting']);
  }
}
