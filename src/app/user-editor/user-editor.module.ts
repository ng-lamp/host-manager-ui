import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { UserEditorComponent } from './user-editor.component';
import { UserEditorRoutingModule } from './user-editor-routing.module';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MomentDatePipe } from '../utils/moment-date.pipe';

@NgModule({
  imports: [
    CommonModule,
    UserEditorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AlertModule.forRoot(),
    ModalModule.forRoot(),
  ],
  declarations: [UserEditorComponent, MomentDatePipe],
})
export class UserEditorModule {}
