import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../shared/entities/user.entity';

import { RequestQueryBuilder, CondOperator } from '@nestjsx/crud-request';
import { abort } from 'process';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class HostingBrowserService {
  constructor(private http: HttpClient) {}

  // return this.http.get<Todo[]>(this.baseUrl, httpOptions).pipe(map((results: any) => results.todos), catchError(this.handleError));

  findAll(): Observable<User[]> {
    const qb = RequestQueryBuilder.create();

    qb.setJoin({ field: 'manager' })
      .setJoin({ field: 'accounts' })
      .setJoin({ field: 'accounts.domains' })
      .setJoin({ field: 'accounts.domains.virtualHosts' })
      .setJoin({ field: 'accounts.domains.virtualHosts.phpFpmPool' });

    let getUrl = environment.apiBaseUrl + `/users?` + qb.query();

    return this.http.get<User[]>(getUrl, httpOptions).pipe(
      map((results: any) => results),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.log(
        `Backend returned code ${error.status}, body was: ${error.status}`
      );
    }
    return throwError(`Something bad happened; please try again later.`);
  }

  // get(id: any): Observable<User> {
  //   return this.http.get(`${baseUrl}/${id}`);
  // }

  // create(data: any): Observable<any> {
  //   return this.http.post(baseUrl, data);
  // }

  // update(id: any, data: any): Observable<any> {
  //   return this.http.put(`${baseUrl}/${id}`, data);
  // }

  // delete(id: any): Observable<any> {
  //   return this.http.delete(`${baseUrl}/${id}`);
  // }
}
