import { ApiProperty } from '@nestjs/swagger';
import { SoftDeleteTimestampedEntity } from './base-entity-extends';
import {
  AfterInsert,
  AfterLoad,
  AfterUpdate,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import {
  Allow,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  Length,
  IsUUID,
} from 'class-validator';
import { Account } from './account.entity';
import { VirtualHost } from './virtual-host.entity';

@Entity({ name: 'domain', withoutRowid: false })
export class Domain extends SoftDeleteTimestampedEntity {
  @Column({
    type: 'int',
    primary: true,
    generated: true,
  })
  id: number;

  @Column({
    type: 'datetime',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  disabledAt: Date;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: false,
    unique: true,
  })
  @ApiProperty({
    description: 'Fully Qualified Domain',
    nullable: false,
    required: true,
    maxLength: 256,
    example: 'example.com',
  })
  @IsNotEmpty()
  name!: string;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: false,
    default: '',
  })
  @ApiProperty({
    description:
      'Treated as absolute path if starts with a slash, otherwise relative to {domain.account.home}',
    default: '',
    maxLength: 256,
    examples: ['/var/www/html', 'example.com'],
  })
  baseDir: string;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    default: false,
    required: false,
  })
  mailService: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    description:
      'Forces https and sets appropriate header to all virtualHosts that has a certificate (Strict-Transport-Security max-age)',
    default: false,
    required: false,
  })
  sslForce: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    description:
      'Adds header: Strict-Transport-Security includeSubDomains (requires sslForce - use with care)',
    default: false,
    required: false,
  })
  sslSubdomainHeader: boolean;

  @Column({
    type: 'int',
  })
  @ApiProperty({
    description: 'Associated Account',
  })
  accountId: number;

  @ManyToOne(() => Account, (account) => account.domains, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'accountId',
  })
  account: Account;

  @OneToMany(() => VirtualHost, (vh) => vh.domain)
  virtualHosts: VirtualHost[];

  // @ApiProperty({
  //   description: 'computed',
  //   readOnly: true,
  //   required: false,
  // })
  // protected isActive: boolean;

  // @AfterLoad()
  // @AfterInsert()
  // @AfterUpdate()
  // async computedData(): Promise<void> {
  //   this.isActive = this.disabledAt === null;
  // }
}
