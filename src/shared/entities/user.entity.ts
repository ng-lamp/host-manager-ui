import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { SoftDeleteTimestampedEntity } from './base-entity-extends';
import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Exclude } from 'class-transformer';
import {
  Allow,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  Length,
  IsUUID,
} from 'class-validator';
import * as bcrypt from 'bcrypt';
import { Account } from './account.entity';
// import { Account } from './account.entity';

@ApiTags('users')
@Entity({ name: 'user', withoutRowid: true })
export class User extends SoftDeleteTimestampedEntity {
  // @PrimaryGeneratedColumn('uuid')
  @Column({
    type: 'uuid',
    primary: true,
    generated: 'uuid',
  })
  @ApiProperty({
    type: 'string',
    format: 'uuid',
  })
  @IsUUID()
  id: string;

  @Column({
    default: true,
  })
  @ApiProperty({
    default: true,
    required: false,
  })
  isActive: boolean;

  @Column({
    nullable: false,
    unique: true,
  })
  @ApiProperty()
  @IsNotEmpty({ message: 'The username is required' })
  @Length(2, 30, {
    message: 'The name must be at least 2 but not longer than 30 characters',
  })
  username!: string;

  @Column({ select: false })
  @Exclude()
  @IsNotEmpty({ message: 'A password is required' })
  password?: string;

  @Column({
    type: 'varchar',
    length: 128,
    nullable: true,
    unique: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    format: 'email',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsEmail({}, { message: 'Incorrect email' })
  email?: string;

  @Column({
    type: 'text',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
    default: null,
  })
  info?: string;

  @Column({
    type: 'varchar',
    length: 32,
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
    default: null,
  })
  phoneLandline?: string;

  @Column({
    type: 'varchar',
    length: 32,
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    required: false,
    nullable: true,
    default: null,
  })
  phoneMobile?: string;

  @Column({
    type: 'uuid',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    nullable: true,
    default: null,
    required: false,
  })
  managerId: string;

  @ManyToOne(() => User, (user) => user.users, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'managerId',
    referencedColumnName: 'id',
  })
  manager: User;

  @OneToMany(() => User, (user) => user.manager)
  users: User[];

  @OneToMany(() => Account, (account) => account.user)
  accounts: Account[];

  @BeforeInsert()
  async hashPassword(pwd = this.password) {
    const saltOrRounds = 10;
    // const salt = await bcrypt.genSalt();

    const hash = await bcrypt.hash(pwd, saltOrRounds);
    this.password = hash;
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }
}
