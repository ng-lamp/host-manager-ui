import { ApiProperty } from '@nestjs/swagger';
import { SoftDeleteTimestampedEntity } from './base-entity-extends';
import { VirtualHost } from './virtual-host.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity({ name: 'php_fpm_pool', withoutRowid: true })
export class PhpFpmPool extends SoftDeleteTimestampedEntity {
  @Column({
    type: 'varchar',
    length: 16,
    primary: true,
  })
  @ApiProperty({
    description: 'PHP version',
    example: '7.4',
    maxLength: 16,
  })
  id: string;

  @Column({
    type: 'datetime',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  disabledAt: Date;

  @Column({
    type: 'varchar',
    length: 256,
    unique: true,
  })
  @ApiProperty({
    description: 'Displayed name',
    maxLength: 256,
    uniqueItems: true,
  })
  name: string;

  @OneToMany(() => VirtualHost, (virtualHost) => virtualHost.phpFpmPool)
  virtualHosts: VirtualHost[];
}
