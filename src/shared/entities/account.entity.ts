// import { ApiProperty } from '@nestjsx/crud/lib/crud';

import { ApiProperty } from '@nestjs/swagger';
import {
  IsDefined,
  IsUUID,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  Length,
} from 'class-validator';
import { Type } from 'class-transformer';
import { Domain } from './domain.entity';
import { SoftDeleteTimestampedEntity } from './base-entity-extends';
import { User } from './user.entity';
import {
  AfterInsert,
  AfterLoad,
  AfterUpdate,
  Column,
  Entity,
  getConnection,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

export enum AccountAccess {
  none = 'none',
  ftp = 'ftp',
  sftpOnly = 'sftpOnly',
  shell = 'shell',
}
@Entity({ name: 'account', withoutRowid: false })
export class Account extends SoftDeleteTimestampedEntity {
  @Column({
    type: 'int',
    primary: true,
    generated: true,
  })
  @ApiProperty({
    type: 'number',
    readOnly: true,
  })
  id: number;

  @Column({
    type: 'datetime',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  disabledAt: Date;

  @Column({
    type: 'date',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'date',
    required: false,
    nullable: true,
    default: null,
  })
  @Type(() => Date)
  expiresAt: Date;

  @Column({
    type: 'varchar',
    length: 32,
    unique: true,
  })
  @ApiProperty({
    description: 'unix account name',
  })
  @IsNotEmpty()
  @Length(2, 32, {
    message: 'The name must be 2 to 32 characters',
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: true,
    default: null,
  })
  @ApiProperty({
    description: 'unix account password',
    nullable: true,
    default: null,
  })
  // @IsNotEmpty({ message: 'A password is required' })
  password: string;

  @Column({
    type: 'varchar',
    length: 256,
  })
  @IsNotEmpty()
  @ApiProperty({
    description: 'unix account home directory.',
    example: '/home/username',
  })
  home: string;

  @Column({
    type: 'simple-enum',
    enum: AccountAccess,
    default: AccountAccess.sftpOnly,
  })
  @ApiProperty({
    type: 'enum',
    description: 'user can access via ftp (+sftp), sftpOnly, shell or none',
    enum: AccountAccess,
    default: AccountAccess.sftpOnly,
    required: false,
  })
  remoteAccess: AccountAccess;

  @Column({
    type: 'boolean',
    default: true,
  })
  @ApiProperty({
    required: false,
    default: true,
    description: 'MySql account with wildcard privileges (name_%)',
  })
  mysqlAccess: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    required: false,
    default: false,
    description: 'Postgresql account',
  })
  postgresAccess: boolean;

  @Column({
    type: 'uuid',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    format: 'uuid',
    required: false,
    default: null,
    description: 'Associated User',
  })
  @IsUUID()
  userId!: string;

  @ManyToOne(() => User, (user) => user.accounts, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'userId',
    referencedColumnName: 'id',
  })
  user: User;

  @OneToMany(() => Domain, (domain) => domain.account)
  domains: Domain[];

  // @ApiProperty({
  //   description: 'computed',
  //   readOnly: true,
  //   required: false,
  // })
  // protected _isActive: boolean;

  // @AfterLoad()
  // @AfterInsert()
  // @AfterUpdate()
  // async computedData(): Promise<void> {
  //   const user = await getConnection()
  //     .getRepository(User)
  //     .createQueryBuilder()
  //     .select()
  //     .where({ id: this.userId })
  //     .getOne();

  //   this._isActive =
  //     this.disabledAt === null && (user === undefined || user.isActive);
  // }
}
