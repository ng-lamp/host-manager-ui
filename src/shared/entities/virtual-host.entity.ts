import { ApiProperty } from '@nestjs/swagger';
import { Account } from './account.entity';
import { Domain } from './domain.entity';
import { PhpFpmPool } from './php-fpm-pool.entity';
import { SoftDeleteTimestampedEntity } from './base-entity-extends';
import { User } from './user.entity';
import {
  AfterInsert,
  AfterLoad,
  AfterUpdate,
  Column,
  Entity,
  getConnection,
  JoinColumn,
  ManyToOne,
  Unique,
} from 'typeorm';

export enum VirtualHostBackend {
  apache = 'apache',
  nginx = 'nginx',
  custom = 'custom',
}

export class VirtualHostBackendCustom {
  @ApiProperty()
  name: string;

  @ApiProperty({
    example: 'http://localhost:3000',
  })
  upstream: string;

  @ApiProperty()
  declaration: string;
}
@Entity({ name: 'virtual_host', withoutRowid: false })
@Unique('unique_constrain_vhostName_domain', ['domainId', 'name'])
export class VirtualHost extends SoftDeleteTimestampedEntity {
  @Column({
    type: 'int',
    primary: true,
    generated: true,
  })
  id: number;

  @Column({
    type: 'datetime',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: Date,
    nullable: true,
    default: null,
    required: false,
  })
  disabledAt: Date;

  @Column({
    type: 'varchar',
    length: 256,
    default: 'www',
  })
  @ApiProperty({
    description:
      'Trailing dot prevents appending domain.name (similar to dns conf)',
    maxLength: 256,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 256,
    default: 'www',
  })
  @ApiProperty({
    description:
      'If empty, defaults to vh.name - If starts with slash seen as absolute path, otherwise relative to virtualHost.domain.baseDir',
    default: 'www',
    required: false,
    examples: ['/var/www/html', 'www'],
  })
  baseDir: string;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: true,
    default: null,
  })
  @ApiProperty({
    required: false,
    default: null,
    maxLength: 256,
    description: 'See nginx return. (default 302 temp status)',
    examples: [
      'https://example.com$request_uri',
      '301 https://permanent.redirect/$host',
      '410',
    ],
  })
  redirectTo: string;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: true,
    default: null,
  })
  @ApiProperty({
    description:
      "space separated - without final dot (.) appends domain.name - '@' maps to domain.name",
    required: false,
    maxLength: 256,
  })
  alias: string;

  @Column({
    type: 'varchar',
    length: 256,
    nullable: true,
    default: null,
  })
  @ApiProperty({
    description:
      "space separated - without final dot (.) appends domain.name - '@' maps to domain.name",
    default: null,
    required: false,
    maxLength: 256,
  })
  redirectAlias: string;

  @Column({
    type: 'simple-enum',
    enum: VirtualHostBackend,
    default: VirtualHostBackend.apache,
  })
  @ApiProperty({
    type: 'enum',
    enum: VirtualHostBackend,
    default: VirtualHostBackend.apache,
    required: false,
  })
  backend: VirtualHostBackend;

  @Column({
    type: 'simple-json',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: VirtualHostBackendCustom,
    nullable: true,
    default: null,
    required: false,
  })
  backendCustom: VirtualHostBackendCustom;

  @Column({
    type: 'text',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    format: 'text',
    nullable: true,
    default: null,
    required: false,
    description: 'Remplaces content of nginx virtual host configuration.',
  })
  confNginx: string;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
    description: 'Remplace all of nginx virtual host configuration.',
  })
  confNginxOverride: boolean;

  @Column({
    type: 'text',
    nullable: true,
    default: null,
  })
  @ApiProperty({
    type: 'string',
    format: 'text',
    nullable: true,
    default: null,
    required: false,
    description: 'Remplaces content of apache virtual host configuration.',
  })
  confApache: string;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
    description: 'Remplace all of apache virtual host configuration.',
  })
  confApacheOverride: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
  })
  varnishCache: boolean;

  @Column({
    type: 'boolean',
    default: true,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
    description: 'certbot keys are available',
  })
  sslCertbot: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
    description: 'Takes effect when hasSslCert',
  })
  sslEnabled: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
    description: 'Note that Domain.sslForce takes precedence',
  })
  sslForce: boolean;

  @Column({
    type: 'boolean',
    default: true,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: true,
    description: "When sslEnabled, adds 'Secure;' to set-cookie headers",
  })
  secureCookie: boolean;

  @Column({
    type: 'boolean',
    default: true,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: true,
    description: 'When sslEnabled, adds NoSniff headers',
  })
  secureNoSniff: boolean;

  @Column({
    type: 'boolean',
    default: true,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: true,
    description: 'Remove fbclid from uri. (uses 301 redirects)',
  })
  removeTracer: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    type: 'boolean',
    required: false,
    default: false,
    description:
      'Is a wordpress at site root. (Enables varnish wordpress rules)',
  })
  isWordpress: boolean;

  @Column({
    type: 'boolean',
    default: false,
  })
  @ApiProperty({
    description: 'intercepts access to robots.txt and blocks UserAgent ~* bot',
    required: false,
    default: false,
  })
  banBots: boolean;

  @Column({
    type: 'varchar',
    length: 16,
    nullable: true,
  })
  @ApiProperty({
    description: 'PHP version phpFpmPool.id - null disables php',
    maxLength: 16,
    nullable: true,
    required: false,
  })
  phpFpmPoolId: string;

  @ManyToOne(() => PhpFpmPool, (phpFpmPool) => phpFpmPool.virtualHosts, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'phpFpmPoolId',
  })
  phpFpmPool: PhpFpmPool;

  @Column({
    type: 'int',
  })
  @ApiProperty({
    description: 'Associated Domain.id',
  })
  domainId: number;

  @ManyToOne(() => Domain, (domain) => domain.virtualHosts, {
    onDelete: 'RESTRICT',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'domainId',
  })
  domain: Domain;

  // @ApiProperty({
  //   description: 'computed',
  //   readOnly: true,
  //   required: false,
  // })
  // protected _isActive: boolean;

  // @AfterLoad()
  // @AfterInsert()
  // @AfterUpdate()
  // async computedData(): Promise<void> {
  //   const domain = await getConnection()
  //     .getRepository(Domain)
  //     .createQueryBuilder()
  //     .select()
  //     .leftJoinAndSelect('account', 'account')
  //     .leftJoinAndSelect('account.user', 'account_user')
  //     .where({ id: this.id })
  //     .getOne();

  //   this._isActive =
  //     this.disabledAt === null &&
  //     (domain === undefined ||
  //       (domain.disabledAt === null &&
  //         (domain.account === undefined ||
  //           ((domain.account.expiresAt === null ||
  //             domain.account.expiresAt > new Date()) &&
  //             domain.account.user.isActive))));
  // }
}
