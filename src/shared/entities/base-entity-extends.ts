import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  BaseEntity,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export class TimestampedEntity extends BaseEntity {
  /**
   * Auto-generated at insert
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @CreateDateColumn({ type: 'datetime' })
  @Type(() => Date)
  createdAt?: Date;

  /**
   * Auto-updated at insert & update
   *
   * @type {Date}
   * @memberof ControlPlace
   */

  @ApiProperty({
    default: 'CURRENT_TIMESTAMP',
    readOnly: true,
    required: false,
  })
  @UpdateDateColumn({ type: 'datetime' })
  @Type(() => Date)
  updatedAt?: Date;
}

export class SoftDeleteEntity extends BaseEntity {
  @DeleteDateColumn({ type: 'datetime' })
  @Type(() => Date)
  deletedAt?: Date;
}

export class SoftDeleteTimestampedEntity extends TimestampedEntity {
  @ApiProperty({
    type: Date,
    readOnly: true,
    required: false,
    default: null,
  })
  @DeleteDateColumn({ type: 'datetime' })
  @Type(() => Date)
  deletedAt?: Date;
}
