const isAbsPath = /^\//;
const trailingDot = /\.$/;
const trailingSlash = /\/$/;
const dblSlash = /\/+/g;
const relativeToHome = /^[~](\/|$)/; // starts with '~(/)'
const relativeToDomainBaseDir = /^[@.](\/|$)/; // starts with '@(/)' or '.(/)'
const domainBaseDirDynamic = /^[@](\/|$)/; // starts with '@(/)'
const domainRelativeToHome = /^[~.](\/|$)/; // starts with '~(/)' or '.(/)'
